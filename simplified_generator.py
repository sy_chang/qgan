#Quantum Generator simplified for 1D

# This code is part of Qiskit.
#
# (C) Copyright IBM 2019, 2020.
#
# This code is licensed under the Apache License, Version 2.0. You may
# obtain a copy of this license in the LICENSE.txt file in the root directory
# of this source tree or at http://www.apache.org/licenses/LICENSE-2.0.
#
# Any modifications or derivative works of this code must retain this
# copyright notice, and modified files need to carry a notice indicating
# that they have been altered from the originals.

"""Quantum Generator."""

from typing import Optional, List, Union, Dict, Any
from copy import deepcopy
import warnings

import numpy as np

from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit
from qiskit.circuit.library import TwoLocal
from qiskit.aqua import aqua_globals
from qiskit.aqua.components.optimizers import ADAM
from qiskit.aqua.components.optimizers import Optimizer
from qiskit.aqua.components.uncertainty_models import UnivariateVariationalDistribution, \
    MultivariateVariationalDistribution

from math import pi


# pylint: disable=invalid-name


class QuantumGenerator():
    def __init__(self,
                 bounds: np.ndarray,
                 num_qubits: List[int],
                 generator_circuit: Optional[Union[UnivariateVariationalDistribution,
                                                   MultivariateVariationalDistribution,
                                                   QuantumCircuit]] = None,
                 init_params: Optional[Union[List[float], np.ndarray]] = None,
                 optimizer: Optional[Optimizer] = None,
                 snapshot_dir: Optional[str] = None) -> None:


        super().__init__()

        self._bounds = bounds
        self._num_qubits = num_qubits
        self.generator_circuit = generator_circuit
        if generator_circuit is None:
            circuit = QuantumCircuit(sum(num_qubits))
            circuit.h(circuit.qubits)
            var_form = TwoLocal(sum(num_qubits), 'ry', 'cz', reps=1, entanglement='circular')
            circuit.compose(var_form, inplace=True)

            # Set generator circuit
            self.generator_circuit = circuit

        if init_params is None:
            init_params = aqua_globals.random.random(self.generator_circuit.num_parameters) * 2e-2

        self._num_parameters = len(init_params)
        self._bound_parameters = init_params

        # Set optimizer for updating the generator network

        if optimizer:
            self._optimizer = optimizer
        else:
            self._optimizer = ADAM(maxiter=1, tol=1e-6, lr=1e-3, beta_1=0.7,
                                   beta_2=0.99, noise_factor=1e-6,
                                   eps=1e-6, amsgrad=True, snapshot_dir=snapshot_dir)


        self._data_grid = np.linspace(bounds[0], bounds[1], (2 ** self._num_qubits[0]))
        self._shots = None
        self._discriminator = None
        self._ret = {}  # type: Dict[str, Any]
    
    def construct_circuit(self, params=None):
        """
        Construct generator circuit.

        Args:
            params (list | dict): parameters which should be used to run the generator.

        Returns:
            Instruction: construct the quantum circuit and return as gate
        """
        if params is None:
            return self.generator_circuit

        if isinstance(params, (list, np.ndarray)):
            params = dict(zip(self._free_parameters, params))

        return self.generator_circuit.assign_parameters(params)

    def get_grad_fn2(self, quantum_instance, discriminator, shots = None ):
        def grad_fn(x_center):
            generated_data, generated_prob = self.get_output(quantum_instance, x_center, shots)
            prediction_generated = discriminator.get_label(generated_data, detach=True)

            num_params = len(x_center)

            params_plus_minus = np.repeat(np.array([x_center]), 2*num_params, axis = 0)

            for i in range(num_params):
                params_plus_minus[i,i] += pi/2
                params_plus_minus[i + num_params, i] -= pi/2

            _, probs = self.get_output(quantum_instance, params_plus_minus, shots)
            
            p_plus = probs[:num_params]
            p_minus = probs[num_params:]

            gradients = []
            for i in range(len(x_center)):
                gradients.append(np.sum(np.array([(p_minus[i][j]-p_plus[i][j])
                                                  *np.log(prediction_generated[j])/2
                                                  for j in range(len(prediction_generated))])))

            return np.array(gradients)
        return grad_fn

    def get_output(self, quantum_instance, params=None, shots=None):
        instance_shots = quantum_instance.run_config.shots

        if shots is not None:
            quantum_instance.set_config(shots=shots)

        if params is None:
            params = np.array(self._bound_parameters)
        elif type(params) == list : 
            params = np.array(params)

        if len(params.shape) == 1 :
            params = np.reshape(params, (1, len(params)))

        circuits = []

        for p in params : 
            q = QuantumRegister(sum(self._num_qubits), name='q')
            qc = QuantumCircuit(q)

            
            qc.append(self.construct_circuit(p), q)
            if quantum_instance.is_statevector:
                pass
            else:
                c = ClassicalRegister(sum(self._num_qubits), name='c')
                qc.add_register(c)
                qc.measure(q, c)
            
            circuits.append(qc)

        job = quantum_instance.execute(circuits)

        generated_samples = []
        generated_samples_weights = []

        if quantum_instance.is_statevector:
            for qc in circuits : 
                result = job.get_statevector(qc)
                values = np.multiply(result, np.conj(result))
                values = list(values.real)
                generated_samples_weights.append(values)
        else:
            for qc in circuits :  
                result = job.get_counts(qc)
                keys = list(result)
                temp = list(result.values())
                temp = [float(v) / np.sum(temp) for v in temp]
                values = [0.0 for _ in range(2**self._num_qubits[0])]

                for j, b in enumerate(keys) :
                    values[int(b, 2)] = temp[j]
                generated_samples_weights.append(values)

        if len(generated_samples_weights) == 1:
            generated_samples_weights = generated_samples_weights[0]
        

        generated_samples = [[x] for x in self._data_grid]

        # self.generator_circuit._probabilities = generated_samples_weights
        if shots is not None:
            # Restore the initial quantum_instance configuration
            quantum_instance.set_config(shots=instance_shots)
        return generated_samples, generated_samples_weights



    def loss(self, x, weights):  # pylint: disable=arguments-differ
        """
        Loss function for training the generator's parameters.

        Args:
            x (numpy.ndarray): sample label (equivalent to discriminator output)
            weights (numpy.ndarray): probability for measuring the sample

        Returns:
            float: loss function
        """
        try:
            # pylint: disable=no-member
            loss = (-1) * np.dot(np.log(x).transpose(), weights)
        except Exception:  # pylint: disable=broad-except
            loss = (-1) * np.dot(np.log(x), weights)
        return loss.flatten()

    def _get_objective_function(self, quantum_instance, discriminator):
        def objective_function(params):
            generated_data, generated_prob = self.get_output(quantum_instance, params=params,
                                                             shots= self._shots)
            prediction_generated = discriminator.get_label(generated_data, detach=True)
            return self.loss(prediction_generated, generated_prob)

        return objective_function

    def train(self, discriminator, quantum_instance=None, shots=None, PS = False):
        self._shots = shots
        objective = self._get_objective_function(quantum_instance, discriminator)

        if PS :
            grad_fn = self.get_grad_fn(quantum_instance, discriminator, shots)

            self._bound_parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._bound_parameters),
                objective_function=objective,
                gradient_function = grad_fn,
                initial_point=self._bound_parameters
                )
        else :
            self._bound_parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._bound_parameters),
                objective_function=objective,
                initial_point=self._bound_parameters
                )

        self._ret['loss'] = loss
        self._ret['params'] = self._bound_parameters

        return self._ret
    

    
    
