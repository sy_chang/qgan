import argparse
import os
from numpy import flip

import yaml
from tools import *
from qgan import *

if __name__ == "__main__":
    

    parser = argparse.ArgumentParser(
        description="Run qGAN experiments on simulator")
    parser.add_argument('--config',  '-c',
                        dest="filename",
                        metavar='FILE',
                        help =  'path to the config file',
                        default='configs/training.yaml')

    args = parser.parse_args()

    with open(args.filename, 'r') as file:
        try:
            config = yaml.safe_load(file)
        except yaml.YAMLError as exc:
            print(exc)
    
    backend_name = config['training_params']['backend_name']
    num_shots = config['training_params']['num_shots']
    num_epochs = config['training_params']['num_epochs']
    shift_rule = config['training_params']['shift_rule']
    init_distribution = config['training_params']['init_distribution']

    # ro_noise_only = config['noise_params']['readout_noise_only']
    # flip_proba = config['noise_params']['flip_proba']
    # cx_error = config['noise_params']['cx_error']

    error_mitigation = config['mitigation_params']['error_mitigation']

    lrate_g = config['hyperparameters']['lrate_g']
    lrate_d = config['hyperparameters']['lrate_d']
    decay_rate = config['hyperparameters']['decay_rate']

    save_dir = config['logging_params']['save_dir']


    print("Training qGAN using")
    print(" - num_epochs:        ", num_epochs)
    print(" - backend:           ", backend_name)
    print(" - shift_rule:        ", shift_rule)
    print(" - init_distribution: ", init_distribution)
    print(" - lrate_g:           ", lrate_g)
    print(" - lrate_d:           ", lrate_d)
    print(" - decay_rate:        ", decay_rate)

    if backend_name != 'statevector_simulator':
        print(" - num_shots:         ", num_shots)
        print(" - error_mitigation:  ", error_mitigation)
        
        if backend_name == 'qasm_simulator' :
            if "flip_proba" in config['noise_params'].keys(): 
                print(" - flip_proba:        ", config['noise_params']['flip_proba'])
                print(" - cx_error:          ", config['noise_params']['cx_error'])
                
        
        else : 
            print(" - use_noise_model:   ", config['noise_params']["use_noise_model"])
            if  config['noise_params']["use_noise_model"]:
                print(" - only bitflip:      ", config['noise_params']['readout_noise_only'])

    path_count = 1
    snapshot_dir = os.path.join(save_dir, 'run' + str(path_count))
    while os.path.exists(snapshot_dir):
        path_count += 1
        snapshot_dir = os.path.join(save_dir, 'run' + str(path_count))

    os.makedirs(snapshot_dir)

    with open(os.path.join(snapshot_dir, "summary.txt"), "+w") as f: 
        f.write(" - num_epochs:        " + str(num_epochs) + "\n")
        f.write(" - backend:           " + backend_name + "\n")
        f.write(" - shift_rule:        " + str(shift_rule) + "\n")
        f.write(" - init_distribution: " + str(init_distribution) + "\n")
        f.write(" - lrate_g:           " + str(lrate_g) + "\n")
        f.write(" - lrate_d:           " + str(lrate_d) + "\n" )
        f.write(" - decay_rate:        " + str(decay_rate) + "\n")

        if backend_name != 'statevector_simulator':
            f.write(" - num_shots:         "+ str(num_shots) + "\n")
            f.write(" - error_mitigation:  "+ str(error_mitigation) + "\n")

            if backend_name == 'qasm_simulator' :
                if "flip_proba" in config['noise_params'].keys(): 
                    f.write(" - flip_proba:        "+ str(config['noise_params']['flip_proba']) + "\n")
                    f.write(" - cx_error:          "+ str(config['noise_params']['cx_error']) + "\n")
                    
            else : 
                if config['noise_params']['use_noise_model'] : 
                    f.write(" - use noise model;   " + str(config['noise_params']['use_noise_model']) + "\n")
                    if  config['noise_params']["use_noise_model"]:
                        f.write(" - only bitflip:      "+ str(config['noise_params']['readout_noise_only']) + "\n")


    quantum_instance = generate_backend(backend_name, error_mitigation, **config["noise_params"])

    X = load_training_data("C:/Users/suchang/cernbox/Documents/v1_Work/v0_data/Electron2D_data.h5")
    X_train, real_dist, bounds = average_data(X)

    generator, discrimintor = setup_qgan(bounds, 
                distribution = init_distribution, 
                snapshot_dir= snapshot_dir)

    g_loss, d_loss, rel_entr, discriminator, generator = train_qgan(bounds, X_train, real_dist, generator, discrimintor, 
                     num_epochs, quantum_instance, batch_size = 3000,
                     num_shots = num_shots,  parameter_shift_rule = shift_rule, 
                     lrate_g = lrate_g, lrate_d = lrate_d, decay_rate = decay_rate,
                     snapshot_dir= snapshot_dir)

    # Visualize the results
    count = 1

    # if backend_name == 'statevector_simulator':
    #     figname = 'su_yeon_v3_statevector_simulator_' + init_distribution + '_shift_rule_' + str(shift_rule) + '_epochs' + \
    #         str(num_epochs) 
    # else:
    #     if backend_name == 'qasm_simulator':
    #         figname_raw = 'su_yeon_v3_' + backend_name + '_' + init_distribution + '_shift_rule_' + str(shift_rule) + '_epochs' + \
    #             str(num_epochs) + '_shots' + str(num_shots) 
    #     elif ro_noise_only:
    #         figname_raw = 'su_yeon_v3_' + backend_name + '_readout_noise_only_' + init_distribution + '_shift_rule_' + str(shift_rule) + '_epochs' + \
    #             str(num_epochs) + '_shots' + str(num_shots) 
    #         figname_raw = 'su_yeon_v3_' + backend_name + '_full_noise_' + init_distribution + '_shift_rule_' + str(shift_rule) + '_epochs' + \
    #             str(num_epochs) + '_shots' + str(num_shots) 
            
    #         if error_mitigation : 
    #             figname_raw += "_mitigated"
        
    # figname_raw += "_run"

    # figname = figname_raw + str(count)
    # while os.path.isfile('garbage/' + figname + '.pdf'):
    #     count += 1
    #     figname = figname_raw + str(count)

    # plot_results(X_train, generator, quantum_instance, g_loss,
    #              d_loss, rel_entr, bounds, num_epochs, figname=figname)