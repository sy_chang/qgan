import warnings

import time
import os
import csv

from scipy.stats import entropy
from math import *
import numpy as np

from qiskit import IBMQ, Aer


from qiskit.aqua import aqua_globals, QuantumInstance
from qiskit.aqua.components.neural_networks.pytorch_discriminator import PyTorchDiscriminator
from qiskit.aqua import aqua_globals
from qiskit.aqua.components.optimizers import ADAM
from qiskit.circuit.library import TwoLocal, UniformDistribution, NormalDistribution

import qiskit.providers.aer.noise as noise
from qiskit.providers.aer.noise import NoiseModel
from qiskit.ignis.mitigation.measurement import CompleteMeasFitter


from simplified_generator import QuantumGenerator
from tools import *

def build_error_matrix(flip_probabilities):
    """
    Given a list of with flip probabilities [p_q0, p_q1] for the individual
    qubits q=1...N, determine the corresponding matrix of flip probabilities.
    """
    
    # Determine the number of qubits         
    N = len(flip_probabilities)    
    
    # Iterate over the qubits from N-1 to 0 as Qiskit orders qubits according to q_{N-1}, q_{N-2}, ..., q_0
    first = True
    for q in range(N-1,-1,-1):
        # Construct a matrix for the readout error
        p = flip_probabilities[q]
        ro_matrix_qubit_q = np.zeros((2,2))
        ro_matrix_qubit_q[0,0] = 1 - p[0]
        ro_matrix_qubit_q[0,1] = p[0]
        ro_matrix_qubit_q[1,0] = p[1]
        ro_matrix_qubit_q[1,1] = 1 - p[1]
        if first:
            ro_error_tot = noise.ReadoutError(ro_matrix_qubit_q)
            first = False
        else:            
            ro_error_tot = ro_error_tot.tensor(noise.ReadoutError(ro_matrix_qubit_q))
            
    return ro_error_tot


#
#
#  use_noise_model = True, readout_noise_only=False, noise_dict = None
def generate_backend(backend_name, error_mitigation, **kwargs):
    """
    Generate an instance of a backend which can either be the state vector simulator, the qasm simulator with various noise models 
    """
    # Set quantum instance to run the quantum generator
    if backend_name == 'statevector_simulator':
        # The state-vector simulator
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('statevector_simulator'))
    elif backend_name == 'qasm_simulator':
        
        if 'flip_proba' not in kwargs and 'cx_error' not in kwargs :
            # The qasm simulator mimicing a perfect quantum computer
            quantum_instance = QuantumInstance(
                backend=Aer.get_backend('qasm_simulator'))
        else : 
            flip_probabilities = kwargs['flip_proba']
            cx_error = kwargs['cx_error']

            noise_model = noise.NoiseModel()


            if flip_probabilities is not None :
                ro_error = build_error_matrix(flip_probabilities)

                if len(flip_probabilities) == 3 : 
                    noise_model.add_readout_error(ro_error, [0,1,2])
                else : 
                    noise_model.add_all_qubit_readout_error(ro_error)

            if cx_error != 0.0 : 
                #depolarizing error
                error = noise.depolarizing_error(cx_error, 2)
                noise_model.add_all_qubit_quantum_error(error, ['cx', 'cz'])
            
            basis_gates = noise_model.basis_gates
            coupling_map = [[0, 1], [1, 0], [1, 2], [1, 3], [2, 1], [3, 1], [3, 4], [4, 3]]
            
            quantum_instance = QuantumInstance(backend=Aer.get_backend(
                'qasm_simulator'), coupling_map = coupling_map,
                 basis_gates=basis_gates, noise_model=noise_model)
            if error_mitigation : 
                quantum_instance = QuantumInstance(backend=Aer.get_backend(
                    'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model,
                    measurement_error_mitigation_cls=CompleteMeasFitter)
            else : 
                quantum_instance = QuantumInstance(backend=Aer.get_backend(
                    'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)

    else : 
        IBMQ.load_account()

        provider = IBMQ.get_provider(hub='ibm-q')
        hardware_backend = provider.get_backend(backend_name)

        if kwargs["use_noise_model"] : 
            if kwargs["readout_noise_only"]:
                # We mimic a noisy backend but we only keep readout errors
                noise_model = NoiseModel.from_backend(hardware_backend)
                coupling_map = hardware_backend.configuration().coupling_map
                basis_gates = noise_model.basis_gates
                # After retrieving the full noise model, we serialize it into a dictionary and delete any noise that is not readout noise
                noise_model_dict = noise_model.to_dict()
                # This dictionary should have two entries with key "errors", "x90_gates", the second one is typically empty and I have no idea what it is good for
                if ('x90_gates' in noise_model_dict) and noise_model_dict['x90_gates']:
                    warnings.warn(
                        "Entry for x_90_gates in noise model dictionary not empty!")
                # We now examine the list of dictionaries specifying the errors and only keep those which are related to readout error
                tmp = list()
                for entry in noise_model_dict['errors']:
                    if entry['type'] == 'roerror':
                        # We found an entry corresponding to a readout error, keep it
                        tmp.append(entry)
                noise_model_dict['errors'] = tmp
                noise_model = NoiseModel.from_dict(noise_model_dict)
                
            else :        
                # We mimic a noisy backend
                noise_model = NoiseModel.from_backend(hardware_backend)
                coupling_map = hardware_backend.configuration().coupling_mabip
                basis_gates = noise_model.basis_gates
                quantum_instance = QuantumInstance(backend=Aer.get_backend(
                    'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)
        else : 
            print("use real hardware")
            quantum_instance = QuantumInstance(backend = hardware_backend)
            
    return quantum_instance


def setup_qgan(bounds, num_qubits=[3], depth=2, distribution="uniform", snapshot_dir=None):
    "Set up the quantum generator and the classical discriminator"

    # Set entangler map
    # In this example, we choose linear entangler map
    entangler_map = 'linear'

    # Set an initial state for the generator circuit
    if distribution == 'uniform':
        print('Using uniform initial distribution')
        init_dist = UniformDistribution(sum(num_qubits))
    else:
        print('Using normal initial distribution')
        init_dist = NormalDistribution(
            sum(num_qubits), mu=4, sigma=1.5, bounds=bounds)  # Normal distribution

    # Set RY variational form with chosen initial state
    var_form = TwoLocal(int(np.sum(num_qubits)), rotation_blocks='ry',
                        entanglement_blocks='cz', entanglement=entangler_map, reps=depth)

    # Set generator's initial parameters
    init_params = np.random.rand(var_form.num_parameters_settable) * 0.2 - 0.1

    # Set generator circuit by adding the initial distribution in front of the ansatz
    g_circuit = var_form.compose(init_dist, front=True)

    # The optimizer we are going to use
    optim_g = ADAM(maxiter=1, tol=1e-6, lr=1e-4, beta_1=0.7, beta_2=0.99,
                   noise_factor=1e-6, eps=1e-6, amsgrad=True, snapshot_dir=snapshot_dir)

    # Create a quantum generator
    generator = QuantumGenerator(
        bounds, num_qubits, g_circuit, init_params, optim_g, snapshot_dir)
    generator._free_parameters = sorted(
        g_circuit.parameters, key=lambda p: p.name)

    # Set classical discriminator neural network
    discriminator = PyTorchDiscriminator(len(num_qubits))

    return generator, discriminator

def l_dec(initial_lrate, epoch, start_decay=500, decay_rate=0.001):
    epoch = epoch - 1 #because training starts at epoch 1
    if epoch < start_decay:
        k = 0.0
    else:
        k = decay_rate #0.07
        epoch = epoch - start_decay
    lrate = initial_lrate * np.exp(-k*epoch)
    return lrate

def train_qgan(bounds, X_train, real_dist, generator, discriminator,
             num_epochs, quantum_instance, batch_size=3000, num_shots=-1,
             parameter_shift_rule=True, lrate_g = 1E-4, lrate_d = 1E-4, decay_rate = 0, snapshot_dir=None):
    "Train the QGAN"

    # If we have a negative number of shots that indicates that we are using the same number of shots as the batch size
    if num_shots < 0:
        num_shots = batch_size

    # Lists for monitoring the loss functions and the relative entropy
    d_loss = []
    g_loss = []
    rel_entr = []

    # Take a snapshot
    if snapshot_dir is not None:
        with open(os.path.join(snapshot_dir, 'output.csv'), mode='w') as csv_file:
            fieldnames = ['epoch', 'loss_discriminator', 'loss_generator', 'params_generator', 'generated_prob',
                          'rel_entropy']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

    # Run qGAN
    start = time.time()
    start_decay = 100

#    for e in tqdm(range(num_epochs)):
    for e in range(num_epochs):
        aqua_globals.random.shuffle(X_train)
        index = 0

        generator._optimizer._lr = l_dec(lrate_g, e, start_decay=start_decay, decay_rate=decay_rate)
        discriminator._optimizer._lr = l_dec(lrate_d, e, start_decay=start_decay, decay_rate=decay_rate)


        generated_batch, generated_prob = generator.get_output(
            quantum_instance, shots=num_shots)
        
        while (index + batch_size) <= len(X_train):
            real_batch = X_train[index: index + batch_size]
            index += batch_size

            # 1. Train Discriminator
            ret_d = discriminator.train([real_batch, generated_batch], [
                                        np.ones(len(real_batch)) / len(real_batch), generated_prob])
            d_loss_min = ret_d['loss']

            # 2. Train Generator
            ret_g = generator.train(
                discriminator, quantum_instance, shots=num_shots, PS=parameter_shift_rule)
            g_loss_min = ret_g['loss']

            generated_batch, generated_prob = generator.get_output(
                quantum_instance, shots=num_shots)

        d_loss.append(float(d_loss_min))
        g_loss.append(g_loss_min)
        rel = entropy(
            np.array([1e-8 if x == 0 else x for x in generated_prob]), real_dist)
        rel_entr.append(rel)

        if snapshot_dir is not None:
            with open(os.path.join(snapshot_dir, 'output.csv'), mode='a') as csv_file:
                fieldnames = ['epoch', 'loss_discriminator',
                              'loss_generator', 'params_generator', 'generated_prob', 'rel_entropy']
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                writer.writerow({'epoch': e, 'loss_discriminator': np.average(np.around(d_loss_min, 4)),
                                 'loss_generator': np.average(np.around(g_loss_min, 4)),
                                 'params_generator': generator._bound_parameters,
                                 'generated_prob': generated_prob,
                                 'rel_entropy': np.around(rel, 4)})
            discriminator.save_model(snapshot_dir)  # Store discriminator model

        #plots
        if snapshot_dir is not None : 
            if e%1 == 0:
                loss_plots(snapshot_dir, e+1, g_loss, d_loss)
                relative_entropy_plot(snapshot_dir, rel_entr, e+1)
                PDF_plot(snapshot_dir, generator, quantum_instance, X_train, bounds, e+1)

        print('Epoch %s/%s, D_loss: %.4f, G_loss: %.4f, Rel_entr: %.4f' % (e + 1, num_epochs,
                                                                       np.around(float(d_loss_min), 4), np.around(g_loss_min, 4), np.around(rel, 4)))

    end = time.time()
    # Runtime
    print('qGAN training runtime: ', (end - start)/60., ' min')

    return g_loss, d_loss, rel_entr, discriminator, generator

