from __future__ import print_function
from collections import defaultdict
try:
    import cPickle as pickle
except ImportError:
    import pickle
import argparse
import os
#os.environ['LD_LIBRARY_PATH'] = os.getcwd()
#print(os.environ)
from six.moves import range
import sys
import h5py 
import numpy as np
#from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  #I need this for my plot

from qiskit import IBMQ, Aer
from qiskit.providers.aer.noise import NoiseModel
import qiskit.providers.aer.noise as noise
from qiskit.utils import QuantumInstance
from qiskit.ignis.mitigation.measurement import complete_meas_cal, CompleteMeasFitter



def build_error_matrix(flip_probabilities):
    """
    Given a list of with flip probabilities [p_q0, p_q1] for the individual
    qubits q=1...N, determine the corresponding matrix of flip probabilities.
    """
    
    # Determine the number of qubits         
    N = len(flip_probabilities)    
    
    # Iterate over the qubits from N-1 to 0 as Qiskit orders qubits according to q_{N-1}, q_{N-2}, ..., q_0
    first = True
    for q in range(N-1,-1,-1):
        # Construct a matrix for the readout error
        p = flip_probabilities[q]
        ro_matrix_qubit_q = np.zeros((2,2))
        ro_matrix_qubit_q[0,0] = 1 - p[0]
        ro_matrix_qubit_q[0,1] = p[0]
        ro_matrix_qubit_q[1,0] = p[1]
        ro_matrix_qubit_q[1,1] = 1 - p[1]
        if first:
            ro_error_tot = noise.ReadoutError(ro_matrix_qubit_q)
            first = False
        else:            
            ro_error_tot = ro_error_tot.tensor(noise.ReadoutError(ro_matrix_qubit_q))
            
    return ro_error_tot


def generate_backend(backend_name, readout_noise_only=False, flip_probabilities = None, error_mitigation = False):
    """
    Generate an instance of a backend which can either be the state vector simulator, the qasm simulator with various noise models 
    """
    # Set quantum instance to run the quantum generator
    if backend_name == 'statevector_simulator':
        # The state-vector simulator
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('statevector_simulator'))
    elif backend_name == 'qasm_simulator':
        # The qasm simulator mimicing a perfect quantum computer
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('qasm_simulator'))
    elif readout_noise_only:

        if flip_probabilities == None : 
            # We mimic a noisy backend but we only keep readout errors
            IBMQ.load_account()
            provider = IBMQ.get_provider(
                hub='ibm-q')
            hardware_backend = provider.get_backend(backend_name)
            noise_model = NoiseModel.from_backend(hardware_backend)
            coupling_map = hardware_backend.configuration().coupling_map
            basis_gates = noise_model.basis_gates
            # After retrieving the full noise model, we serialize it into a dictionary and delete any noise that is not readout noise
            noise_model_dict = noise_model.to_dict()
            # This dictionary should have two entries with key "errors", "x90_gates", the second one is typically empty and I have no idea what it is good for
            if ('x90_gates' in noise_model_dict) and noise_model_dict['x90_gates']:
                warnings.warn(
                    "Entry for x_90_gates in noise model dictionary not empty!")
            # We now examine the list of dictionaries specifying the errors and only keep those which are related to readout error
            tmp = list()
            for entry in noise_model_dict['errors']:
                if entry['type'] == 'roerror':
                    # We found an entry corresponding to a readout error, keep it
                    tmp.append(entry)
            noise_model_dict['errors'] = tmp
            noise_model = NoiseModel.from_dict(noise_model_dict)
            
        else : 
            ro_error = build_error_matrix(flip_probabilities)
            noise_model = noise.NoiseModel()

            if len(flip_probabilities) == 3 : 
                noise_model.add_readout_error(ro_error, [0,1,2])
            else : 
                noise_model.add_all_qubit_readout_error(ro_error)
            basis_gates = noise_model.basis_gates
            coupling_map = None

        if error_mitigation : 
            quantum_instance = QuantumInstance(backend=Aer.get_backend(
                'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model,
                measurement_error_mitigation_cls=CompleteMeasFitter)
        else : 
            quantum_instance = QuantumInstance(backend=Aer.get_backend(
                'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)
                
    else:
        # We mimic a noisy backend
        IBMQ.load_account()
        provider = IBMQ.get_provider(
            hub='ibm-q-research', group='independent-19', project='main')
        hardware_backend = provider.get_backend(backend_name)
        noise_model = NoiseModel.from_backend(hardware_backend)
        coupling_map = hardware_backend.configuration().coupling_map
        basis_gates = noise_model.basis_gates
        quantum_instance = QuantumInstance(backend=Aer.get_backend(
            'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)

    return quantum_instance



def create_folder(folder_name, print_outputs = True):        #create folders in which the trainings progress will be saved
    dirName=folder_name
    try:
        # Create target Directory
        os.mkdir(dirName)
        if print_outputs == True:
            print("Directory " , dirName ,  " Created ") 
    except FileExistsError:
        if print_outputs == True:
            print("Directory " , dirName ,  " already exists")
    return

def loss_plots(save_folder, epoch, g_loss, d_loss):  #timestep = epoch
    # Plot progress w.r.t the generator's and the discriminator's loss function
    t_steps = np.arange(epoch)
    #print(epoch, t_steps, g_loss)
    plt.figure(figsize=(6,5))
    plt.title("Loss Function", fontsize=16)
    plt.plot(t_steps, g_loss, label = "Generator Loss", color = 'mediumvioletred', linewidth = 2)
    plt.plot(t_steps, d_loss, label = "Discriminator Loss", color = 'rebeccapurple', linewidth = 2)
    plt.grid()
    plt.legend(loc = 'best', fontsize=14)
    plt.xlabel('epochs', fontsize=14)
    plt.ylabel('loss', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.savefig(os.path.join(save_folder, "loss_plot.png"))
    if epoch%50 == 0:
        plt.show()
    plt.close()
    return

def relative_entropy_plot(save_folder, rel_entr, epoch):
    # Plot progress w.r.t relative entropy
    t_steps = np.arange(epoch)
    plt.figure(figsize=(6,5))
    plt.title("Relative Entropy ", fontsize=16)
    plt.plot(t_steps, rel_entr, color ='mediumblue', lw=4, ls=':')
    plt.grid()
    plt.xlabel('epochs', fontsize=14)
    plt.ylabel('relative entropy', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.savefig(os.path.join(save_folder, "relative_entropy.png"))
    if epoch%50 == 0:
        plt.show()
    plt.close()
    return

def PDF_plot(save_folder, generator, quantum_instance, X_train, bounds, epoch):
    #Plot the PDF of the resulting distribution against the target distribution
    plt.figure(figsize=(6,5))
    plt.title("PDF", fontsize=16)
    samples_g, prob_g = generator.get_output(quantum_instance, shots=10000)


    r = np.round(X_train)
    r = r[r <= bounds[1]]
    temp = []
    for i in range(int(bounds[1]+1)):
        temp += [np.sum(r==i)]
    r = np.array(temp / sum(temp))

    samples_g = np.array(samples_g)
    samples_g = samples_g.flatten()
    num_bins = len(prob_g)
    plt.bar(samples_g,  prob_g, color='royalblue', width= 0.8, label='qGAN')
    plt.plot(r,'-o', label='Geant4', color='deepskyblue', linewidth=4, markersize=12)

    plt.xticks(np.arange(min(samples_g), max(samples_g)+1, 1.0))
    plt.grid()
    plt.xlabel('x', fontsize=14)
    plt.ylabel('p(x)', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.ylim([0,0.25])
    plt.legend(loc='best', fontsize=14)
    if epoch%100 == 0 or epoch == 1:
        plt.savefig(os.path.join(save_folder, str(epoch) + "PDF_plot.png"))
    if epoch%50 == 0 or epoch == 1:
        plt.show()
    plt.close()
    return


