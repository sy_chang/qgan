import h5py
import warnings
import argparse
import os
import csv
import time

from tqdm import tqdm
from scipy.stats import entropy
from math import *
import numpy as np

from qiskit import IBMQ, Aer
from qiskit.providers.aer.noise import NoiseModel
from qiskit.aqua import aqua_globals, QuantumInstance
from qiskit.aqua.components.initial_states import Custom
from qiskit.aqua.components.neural_networks.pytorch_discriminator import PyTorchDiscriminator
from qiskit.aqua.components.neural_networks.numpy_discriminator import NumPyDiscriminator
from qiskit.aqua import aqua_globals
from qiskit.aqua.components.optimizers import ADAM
from qiskit.aqua.components.optimizers import Optimizer
from qiskit.circuit.library import TwoLocal, UniformDistribution, NormalDistribution

from simplified_generator import QuantumGenerator
from FunctionsQ_v1 import *
import matplotlib.pyplot as plt


def load_training_data(file="/data/suchang/data/Electron2D_data.h5"):
    "Load training data"
    nevt = 10000  # Number of events for training
    d = h5py.File(file, 'r')

    # the data consists of two items, the ECAL shower profile which is 200k x 1 x 25 x 25 and
    # the initial electron energy (name = 'target') that is a uniform distribution between [0,500] GeV.
    # we use only the ECAL shower profile and not the energy as input

    xd = d.get('ECAL')
    nx = xd.shape[2]
    ny = xd.shape[3]
    X = np.array(xd[:nevt, :, :])

    return X


def average_data(X, n=3, N=30000):
    x_sum = []
    for x in X:
        tmp = np.sum(x[0], axis=0)
        x_sum.append([np.sum(tmp[3*i:3*(i+1)])
                      for i in range(floor(len(tmp)/3))])
        if np.sum(x_sum[-1]) != 0:
            x_sum[-1] /= np.sum(x_sum[-1])

    real_dist = np.mean(x_sum, axis=0)
    real_pdf = np.array([np.sum(real_dist[0:i+1])
                         for i in range(len(real_dist))])
    real_pdf[-1] = 1

    # Create training set of scalars following the real energy distribution
    X_train = []
    for i in range(N):
        num = np.argwhere(real_pdf > np.random.uniform(0, 1))[0][0]
        if num == 0:
            X_train.append(np.random.uniform(0, 0.5))
        elif num == 7:
            X_train.append(np.random.uniform(6.5, 7))
        else:
            X_train.append(np.random.uniform(num-0.5, num+0.5))

    X_train = np.array(X_train)

    # Set the data resolution
    # Set upper and lower data values as list of k min/max data values [[min_0,max_0],...,[min_k-1,max_k-1]]
    # In this example, k = 1
    bounds = np.array([0., 2**n - 1])

    return X_train, real_dist, bounds


def setup_qgan(bounds, num_qubits=[3], depth=2, distribution="uniform", snapshot_dir=None):
    "Set up the quantum generator and the classical discriminator"

    # Set entangler map
    # In this example, we choose linear entangler map
    entangler_map = 'linear'

    # Set an initial state for the generator circuit
    if distribution == 'uniform':
        print('Using uniform initial distribution')
        init_dist = UniformDistribution(sum(num_qubits))
    else:
        print('Using normal initial distribution')
        init_dist = NormalDistribution(
            sum(num_qubits), mu=4, sigma=1.5, bounds=bounds)  # Normal distribution

    # Set RY variational form with chosen initial state
    var_form = TwoLocal(int(np.sum(num_qubits)), rotation_blocks='ry',
                        entanglement_blocks='cz', entanglement=entangler_map, reps=depth)

    # Set generator's initial parameters
    init_params = np.random.rand(var_form.num_parameters_settable) * 0.2 - 0.1

    # Set generator circuit by adding the initial distribution in front of the ansatz
    g_circuit = var_form.compose(init_dist, front=True)

    # The optimizer we are going to use
    optim_g = ADAM(maxiter=1, tol=1e-6, lr=1e-4, beta_1=0.7, beta_2=0.99,
                   noise_factor=1e-6, eps=1e-6, amsgrad=True, snapshot_dir=snapshot_dir)

    # Create a quantum generator
    generator = QuantumGenerator(
        bounds, num_qubits, g_circuit, init_params, optim_g, snapshot_dir)
    generator._free_parameters = sorted(
        g_circuit.parameters, key=lambda p: p.name)

    # Set classical discriminator neural network
    discriminator = PyTorchDiscriminator(len(num_qubits))

    return generator, discriminator

def l_dec(initial_lrate, epoch, start_decay=500, decay_rate=0.001):
    epoch = epoch - 1 #because training starts at epoch 1
    if epoch < start_decay:
        k = 0.0
    else:
        k = decay_rate #0.07
        epoch = epoch - start_decay
    lrate = initial_lrate * np.exp(-k*epoch)
    return lrate

def train_qgan(X_train, real_dist, generator, discriminator,
             num_epochs, quantum_instance, batch_size=3000, num_shots=-1,
             parameter_shift_rule=True, lrate_g = 1E-4, lrate_d = 1E-4, decay_rate = 0, snapshot_dir=None):
    "Train the QGAN"

    # If we have a negative number of shots that indicates that we are using the same number of shots as the batch size
    if num_shots < 0:
        num_shots = batch_size

    # Lists for monitoring the loss functions and the relative entropy
    d_loss = []
    g_loss = []
    rel_entr = []

    # Take a snapshot
    if snapshot_dir is not None:
        with open(os.path.join(snapshot_dir, 'output.csv'), mode='w') as csv_file:
            fieldnames = ['epoch', 'loss_discriminator', 'loss_generator', 'params_generator', 'generated_prob',
                          'rel_entropy']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

    # Run qGAN
    start = time.time()
    start_decay = 100

#    for e in tqdm(range(num_epochs)):
    for e in range(num_epochs):
        aqua_globals.random.shuffle(X_train)
        index = 0

        generator._optimizer._lr = l_dec(lrate_g, e, start_decay=start_decay, decay_rate=decay_rate)
        discriminator._optimizer._lr = l_dec(lrate_d, e, start_decay=start_decay, decay_rate=decay_rate)


        generated_batch, generated_prob = generator.get_output(
            quantum_instance, shots=num_shots)
        
        while (index + batch_size) <= len(X_train):
            real_batch = X_train[index: index + batch_size]
            index += batch_size

            # 1. Train Discriminator
            ret_d = discriminator.train([real_batch, generated_batch], [
                                        np.ones(len(real_batch)) / len(real_batch), generated_prob])
            d_loss_min = ret_d['loss']

            # 2. Train Generator
            ret_g = generator.train(
                discriminator, quantum_instance, shots=num_shots, PS=parameter_shift_rule)
            g_loss_min = ret_g['loss']

            generated_batch, generated_prob = generator.get_output(
                quantum_instance, shots=num_shots)

        d_loss.append(float(d_loss_min))
        g_loss.append(g_loss_min)
        rel = entropy(
            np.array([1e-8 if x == 0 else x for x in generated_prob]), real_dist)
        rel_entr.append(rel)

        if snapshot_dir is not None:
            with open(os.path.join(snapshot_dir, 'output.csv'), mode='a') as csv_file:
                fieldnames = ['epoch', 'loss_discriminator',
                              'loss_generator', 'params_generator', 'generated_prob', 'rel_entropy']
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                writer.writerow({'epoch': e, 'loss_discriminator': np.average(np.around(d_loss_min, 4)),
                                 'loss_generator': np.average(np.around(g_loss_min, 4)),
                                 'params_generator': generator._bound_parameters,
                                 'generated_prob': generated_prob,
                                 'rel_entropy': np.around(rel, 4)})
            discriminator.save_model(snapshot_dir)  # Store discriminator model

        #plots
        if e%1 == 0:
            loss_plots(snapshot_dir, e+1, g_loss, d_loss)
            relative_entropy_plot(snapshot_dir, rel_entr, e+1)
            PDF_plot(snapshot_dir, generator, quantum_instance, X_train, bounds, e+1)

        print('Epoch %s/%s, D_loss: %.4f, G_loss: %.4f, Rel_entr: %.4f' % (e + 1, num_epochs,
                                                                       np.around(float(d_loss_min), 4), np.around(g_loss_min, 4), np.around(rel, 4)))

    end = time.time()
    # Runtime
    print('qGAN training runtime: ', (end - start)/60., ' min')

    return g_loss, d_loss, rel_entr, discriminator, generator


def plot_results(data, generator, quantum_instance, g_loss, d_loss, rel_entr, bounds, num_epochs, figname=""):
    "Visualize final results"

    t_steps = np.arange(num_epochs)

    plt.figure(figsize=(18, 5))

    plt.subplot(131)
    t_steps = np.arange(num_epochs)
    plt.title("Progress in the loss function")
    plt.plot(t_steps, g_loss, label="Generator loss function", linewidth=2)
    plt.plot(t_steps, d_loss,
             label="Discriminator loss function", linewidth=2)
    plt.grid()
    plt.legend(loc='best')
    plt.xlabel('time steps')
    plt.ylabel('loss')

    # Plot progress w.r.t relative entropy
    plt.subplot(132)
    plt.title("Relative Entropy ")
    plt.plot(t_steps, rel_entr, lw=2)
    plt.grid()
    plt.xlabel('time steps')
    plt.ylabel('relative entropy')

    # Plot the PDF of the resulting distribution against the target distribution
    plt.subplot(133)
    plt.title("CDF")
    samples_g, prob_g = generator.get_output(quantum_instance, shots=10000)
    r = np.round(data)
    r = r[r <= bounds[1]]
    temp = []
    for i in range(int(bounds[1]+1)):
        temp += [np.sum(r == i)]
    r = np.array(temp / sum(temp))

    samples_g = np.array(samples_g)
    samples_g = samples_g.flatten()
    plt.bar(samples_g,  prob_g, width=0.8, label='simulation')
    plt.plot(r, '--o', label='log-normal', linewidth=2,
             markersize=12, color='tab:orange')

    plt.xticks(np.arange(min(samples_g), max(samples_g)+1, 1.0))
    plt.grid()
    plt.xlabel('x')
    plt.ylabel('p(x)')
    plt.legend(loc='best')

    # Some beautyfication and save final results
    plt.tight_layout()
    if figname:
        plt.savefig('garbage/' + figname + '.pdf')
    # plt.show()
    plt.close("all")


if __name__ == "__main__":
    

    parser = argparse.ArgumentParser(
        description="Run qGAN experiments on simulator")
    parser.add_argument('--backend_name', type=str, default='statevector_simulator',
                        help='the name of the backend we are using/emulating')
    parser.add_argument('--num_shots', type=int, default=-1,
                        help='the number of shots we are going to use in case we emulate a device, a negative value indicates that we take the same number as the batch size (default)')
    parser.add_argument('--num_epochs', type=int, default=1000,
                        help='the number of epochs used for training (default 30)')
    parser.add_argument('--shift_rule', type=int, default=1,
                        help='flag indicating if parameter shift rule is used to compute the gradients or not (default is True)')
    parser.add_argument('--readout_noise_only', type=int, default=0,
                        help='specify if only readout_noise is kept (1) or the full noise model is used (0, default)')
    parser.add_argument('--init_distribution', type=str, default='normal',
                        help='specify the initial distribution that is going to be used (either normal or uniform, default is normal)')
    parser.add_argument('--lrate_g', type=float, default=1E-4,
                        help='learning rate for generator training (default 1E-4)')

    parser.add_argument('--lrate_d', type=float, default=1E-4,
                        help='learning rate for discriminator training (default 1E-4)')

    parser.add_argument('--decay_rate', type=float, default=0,
                        help='learning rate decay (default 0)')

    parser.add_argument('--flip_proba', type=float, default=0,
                        help='flip probability for noise model (default 1E-2)')

    parser.add_argument('--error_mitigation', type=int, default=0,
                        help='apply error mitigation (default 0)')

    args = parser.parse_args()
    # Transform the input into a boolean variable
    args.shift_rule = args.shift_rule == 1
    print("Training qGAN using")
    print(" - num_epochs:        ", args.num_epochs)
    print(" - backend:           ", args.backend_name)
    print(" - shift_rule:        ", args.shift_rule)
    print(" - init_distribution: ", args.init_distribution)
    print(" - lrate_g:           ", args.lrate_g)
    print(" - lrate_d:           ", args.lrate_d)
    print(" - decay_rate:        ", args.decay_rate)

    if args.backend_name != 'statevector_simulator':
        print(" - num_shots:         ", args.num_shots)
        print(" - only bitflip:      ", args.readout_noise_only)
        
        if args.flip_proba != 0.0 : 
            print(" - flip_proba:        ", args.flip_proba)
            print(" - error_mitigation:  ", args.error_mitigation)

    ro_noise_only = False
    if args.readout_noise_only == 1:
        ro_noise_only = True

    error_mitigation = False 
    if args.error_mitigation == 1 : 
        error_mitigation = True 

    path_count = 1
    while os.path.exists('run' + str(path_count)):
        path_count += 1

    os.makedirs('run' + str(path_count))
    with open(os.path.join('run' + str(path_count), "summary.txt"), "+w") as f: 
        f.write(" - num_epochs:        " + str(args.num_epochs) + "\n")
        f.write(" - backend:           " + args.backend_name + "\n")
        f.write(" - shift_rule:        " + str(args.shift_rule) + "\n")
        f.write(" - init_distribution: " + str(args.init_distribution) + "\n")
        f.write(" - lrate_g:           " + str(args.lrate_g) + "\n")
        f.write(" - lrate_d:           " + str(args.lrate_d) + "\n" )
        f.write(" - decay_rate:        " + str(args.decay_rate) + "\n")
        if args.backend_name != 'statevector_simulator':
            f.write(" - num_shots:         "+ str(args.num_shots) + "\n")
            f.write(" - only bitflip:      "+ str(args.readout_noise_only) + "\n")
        
            if args.flip_proba != 0.0 : 
                f.write(" - flip_proba:        "+ str(args.flip_proba) + "\n")
                f.write(" - error_mitigation:  "+ str(args.error_mitigation) + "\n")

    # Get a backend
    p = [[args.flip_proba,args.flip_proba]]
    quantum_instance = generate_backend(args.backend_name, ro_noise_only, flip_probabilities = p, 
                                        error_mitigation = error_mitigation)

    X = load_training_data("C:/Users/suchang/cernbox/Documents/data/Electron2D_data.h5")
    X_train, real_dist, bounds = average_data(X)

    generator, discrimintor = setup_qgan(bounds, 
                distribution = args.init_distribution, 
                snapshot_dir= 'run' + str(path_count))

    g_loss, d_loss, rel_entr, discriminator, generator = train_qgan(X_train, real_dist, generator, discrimintor, 
                     args.num_epochs, quantum_instance, batch_size = 3000,
                     num_shots = args.num_shots,  parameter_shift_rule = args.shift_rule, 
                     lrate_g = args.lrate_g, lrate_d = args.lrate_d, decay_rate = args.decay_rate,
                     snapshot_dir= 'run' + str(path_count))

    # Visualize the results
    count = 1

    if args.backend_name == 'statevector_simulator':
        figname = 'su_yeon_v3_statevector_simulator_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
            str(args.num_epochs) 
    else:
        if args.backend_name == 'qasm_simulator':
            figname_raw = 'su_yeon_v3_' + args.backend_name + '_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots) 
        elif ro_noise_only:
            figname_raw = 'su_yeon_v3_' + args.backend_name + '_readout_noise_only_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots) 
            figname_raw = 'su_yeon_v3_' + args.backend_name + '_full_noise_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots) 
            
            if error_mitigation : 
                figname_raw += "_mitigated"
        
    figname_raw += "_run"

    figname = figname_raw + str(count)
    while os.path.isfile('garbage/' + figname + '.pdf'):
        count += 1
        figname = figname_raw + str(count)

    plot_results(X_train, generator, quantum_instance, g_loss,
                 d_loss, rel_entr, bounds, args.num_epochs, figname=figname)