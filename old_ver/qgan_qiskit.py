import h5py
import warnings
import argparse
import os
import csv
import time

from tqdm import tqdm
from scipy.stats import entropy
from math import *
import numpy as np

from qiskit import IBMQ, Aer
from qiskit.providers.aer.noise import NoiseModel
from qiskit.utils import algorithm_globals, QuantumInstance

from qiskit.algorithms.optimizers import ADAM
from qiskit.circuit.library import TwoLocal

from qiskit_finance.circuit.library.probability_distributions import UniformDistribution, NormalDistribution
from qiskit_machine_learning.algorithms.distribution_learners.qgan import QGAN, QuantumGenerator, PyTorchDiscriminator

from qiskit.opflow import Gradient


import matplotlib.pyplot as plt

def generate_backend(backend_name, readout_noise_only=False):
    """
    Generate an instance of a backend which can either be the state vector simulator, the qasm simulator with various noise models 
    """
    # Set quantum instance to run the quantum generator
    if backend_name == 'statevector_simulator':
        # The state-vector simulator
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('statevector_simulator'))
    elif backend_name == 'qasm_simulator':
        # The qasm simulator mimicing a perfect quantum computer
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('qasm_simulator'))
    elif readout_noise_only:
        # We mimic a noisy backend but we only keep readout errors
        IBMQ.load_account()
        provider = IBMQ.get_provider(
            hub='ibm-q')
        hardware_backend = provider.get_backend(backend_name)
        noise_model = NoiseModel.from_backend(hardware_backend)
        coupling_map = hardware_backend.configuration().coupling_map
        basis_gates = noise_model.basis_gates
        # After retrieving the full noise model, we serialize it into a dictionary and delete any noise that is not readout noise
        noise_model_dict = noise_model.to_dict()
        # This dictionary should have two entries with key "errors", "x90_gates", the second one is typically empty and I have no idea what it is good for
        if ('x90_gates' in noise_model_dict) and noise_model_dict['x90_gates']:
            warnings.warn(
                "Entry for x_90_gates in noise model dictionary not empty!")
        # We now examine the list of dictionaries specifying the errors and only keep those which are related to readout error
        tmp = list()
        for entry in noise_model_dict['errors']:
            if entry['type'] == 'roerror':
                # We found an entry corresponding to a readout error, keep it
                tmp.append(entry)
        noise_model_dict['errors'] = tmp
        noise_model = NoiseModel.from_dict(noise_model_dict)
        quantum_instance = QuantumInstance(backend=Aer.get_backend(
            'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)
    else:
        # We mimic a noisy backend
        IBMQ.load_account()
        provider = IBMQ.get_provider(
            hub='ibm-q-research', group='independent-19', project='main')
        hardware_backend = provider.get_backend(backend_name)
        noise_model = NoiseModel.from_backend(hardware_backend)
        coupling_map = hardware_backend.configuration().coupling_map
        basis_gates = noise_model.basis_gates
        quantum_instance = QuantumInstance(backend=Aer.get_backend(
            'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)

    return quantum_instance


def load_training_data(file="/data/suchang/data/Electron2D_data.h5"):
    "Load training data"
    nevt = 10000  # Number of events for training
    d = h5py.File(file, 'r')

    # the data consists of two items, the ECAL shower profile which is 200k x 1 x 25 x 25 and
    # the initial electron energy (name = 'target') that is a uniform distribution between [0,500] GeV.
    # we use only the ECAL shower profile and not the energy as input

    xd = d.get('ECAL')
    nx = xd.shape[2]
    ny = xd.shape[3]
    X = np.array(xd[:nevt, :, :])

    return X


def average_data(X, n=3, N=30000):
    x_sum = []
    for x in X:
        tmp = np.sum(x[0], axis=0)
        x_sum.append([np.sum(tmp[3*i:3*(i+1)])
                      for i in range(floor(len(tmp)/3))])
        if np.sum(x_sum[-1]) != 0:
            x_sum[-1] /= np.sum(x_sum[-1])

    real_dist = np.mean(x_sum, axis=0)
    real_pdf = np.array([np.sum(real_dist[0:i+1])
                         for i in range(len(real_dist))])
    real_pdf[-1] = 1

    # Create training set of scalars following the real energy distribution
    X_train = []
    for i in range(N):
        num = np.argwhere(real_pdf > np.random.uniform(0, 1))[0][0]
        if num == 0:
            X_train.append(np.random.uniform(0, 0.5))
        elif num == 7:
            X_train.append(np.random.uniform(6.5, 7))
        else:
            X_train.append(np.random.uniform(num-0.5, num+0.5))

    X_train = np.array(X_train)

    # Set the data resolution
    # Set upper and lower data values as list of k min/max data values [[min_0,max_0],...,[min_k-1,max_k-1]]
    # In this example, k = 1
    bounds = np.array([0., 2**n - 1])

    return X_train, real_dist, bounds


def setup_qgan(data, bounds, num_qubits=[3], depth=2, batch_size = 3000, num_epochs = 1000,
                learning_rate=1E-4, distribution="uniform", parameter_shift_rule = False,
                load_dir=None, snapshot_dir=None):
    "Set up the quantum generator and the classical discriminator"

    # Set entangler map
    # In this example, we choose linear entangler map
    entangler_map = 'linear'

    # Set quantum GAN
    qgan = QGAN(data, bounds, num_qubits, batch_size, num_epochs, snapshot_dir=snapshot_dir)

    # Set an initial state for the generator circuit
    if distribution == 'uniform':
        print('Using uniform initial distribution')
        init_dist = UniformDistribution(sum(num_qubits))
    else:
        print('Using normal initial distribution')
        init_dist = NormalDistribution(
            sum(num_qubits), mu=4, sigma=1.5, bounds=bounds)  # Normal distribution

    

    # Set RY variational form with chosen initial state
    var_form = TwoLocal(int(np.sum(num_qubits)), rotation_blocks='ry',
                        entanglement_blocks='cz', entanglement=entangler_map, reps=depth)

    # Set generator's initial parameters
    init_params = np.random.rand(var_form.num_parameters_settable) * 0.2 - 0.1

    # Set generator circuit by adding the initial distribution in front of the ansatz
    g_circuit = var_form.compose(init_dist, front=True)

    # The optimizer we are going to use
    # optim_g = ADAM(maxiter=1, tol=1e-6, lr=1e-4, beta_1=0.7, beta_2=0.99,
                #    noise_factor=1e-6, eps=1e-6, amsgrad=True, snapshot_dir=snapshot_dir)

    optim_g = None

    # Set classical discriminator neural network
    discriminator = PyTorchDiscriminator(len(num_qubits))
    grad_method = "fin_diff"
    if parameter_shift_rule : 
        grad_method = "param_shift"
    print(grad_method)
    # Set quantum generator
    qgan.set_generator(generator_circuit=g_circuit, generator_init_params=init_params, generator_optimizer = optim_g, generator_gradient = Gradient(grad_method))
    qgan.set_discriminator(discriminator)
    # Set learning rates
    qgan._generator._optimizer._lr = learning_rate
    qgan._discriminator._optimizer._lr = learning_rate

    return qgan


def plot_results(data, generator, quantum_instance, g_loss, d_loss, rel_entr, bounds, num_epochs, figname=""):
    "Visualize final results"

    t_steps = np.arange(num_epochs)

    plt.figure(figsize=(18, 5))

    plt.subplot(131)
    t_steps = np.arange(num_epochs)
    plt.title("Progress in the loss function")
    plt.plot(t_steps, g_loss, label="Generator loss function", linewidth=2)
    plt.plot(t_steps, d_loss,
             label="Discriminator loss function", linewidth=2)
    plt.grid()
    plt.legend(loc='best')
    plt.xlabel('time steps')
    plt.ylabel('loss')

    # Plot progress w.r.t relative entropy
    plt.subplot(132)
    plt.title("Relative Entropy ")
    plt.plot(t_steps, rel_entr, lw=2)
    plt.grid()
    plt.xlabel('time steps')
    plt.ylabel('relative entropy')

    # Plot the PDF of the resulting distribution against the target distribution
    plt.subplot(133)
    plt.title("CDF")
    samples_g, prob_g = generator.get_output(quantum_instance, shots=10000)
    r = np.round(data)
    r = r[r <= bounds[1]]
    temp = []
    for i in range(int(bounds[1]+1)):
        temp += [np.sum(r == i)]
    r = np.array(temp / sum(temp))

    samples_g = np.array(samples_g)
    samples_g = samples_g.flatten()
    plt.bar(samples_g,  prob_g, width=0.8, label='simulation')
    plt.plot(r, '--o', label='log-normal', linewidth=2,
             markersize=12, color='tab:orange')

    plt.xticks(np.arange(min(samples_g), max(samples_g)+1, 1.0))
    plt.grid()
    plt.xlabel('x')
    plt.ylabel('p(x)')
    plt.legend(loc='best')

    # Some beautyfication and save final results
    plt.tight_layout()
    if figname:
        plt.savefig('garbage/' + figname + '.pdf')
    # plt.show()
    plt.close("all")


if __name__ == "__main__":
    

    parser = argparse.ArgumentParser(
        description="Run qGAN experiments on simulator")
    parser.add_argument('--backend_name', type=str, default='statevector_simulator',
                        help='the name of the backend we are using/emulating')
    parser.add_argument('--num_shots', type=int, default=-1,
                        help='the number of shots we are going to use in case we emulate a device, a negative value indicates that we take the same number as the batch size (default)')
    parser.add_argument('--num_epochs', type=int, default=1000,
                        help='the number of epochs used for training (default 30)')
    parser.add_argument('--shift_rule', type=int, default=1,
                        help='flag indicating if parameter shift rule is used to compute the gradients or not (default is True)')
    parser.add_argument('--readout_noise_only', type=int, default=0,
                        help='specify if only readout_noise is kept (1) or the full noise model is used (0, default)')
    parser.add_argument('--init_distribution', type=str, default='normal',
                        help='specify the initial distribution that is going to be used (either normal or uniform, default is normal)')
    parser.add_argument('--learning_rate', type=float, default=1E-4,
                        help='learning rate for training (default 1E-4)')

    args = parser.parse_args()
    # Transform the input into a boolean variable
    args.shift_rule = args.shift_rule == 1
    print("Training qGAN using")
    print(" - num_epochs:        ", args.num_epochs)
    print(" - backend:           ", args.backend_name)
    print(" - shift_rule:        ", args.shift_rule)
    print(" - init_distribution: ", args.init_distribution)
    print(" - learning_rate: ", args.learning_rate)

    if args.backend_name != 'statevector_simulator':
        print(" - num_shots:         ", args.num_shots)
        print(" - only bitflip:      ", args.readout_noise_only)

    ro_noise_only = False
    if args.readout_noise_only == 1:
        ro_noise_only = True

    path_count = 1
    while os.path.exists('run' + str(path_count)):
        path_count += 1

    os.makedirs('run' + str(path_count))

    # Get a backend
    quantum_instance = generate_backend(args.backend_name, ro_noise_only)

    X = load_training_data("C:/Users/suchang/cernbox/Documents/data/Electron2D_data.h5")
    X_train, real_dist, bounds = average_data(X)

    qgan = setup_qgan(X_train, bounds, num_epochs = args.num_epochs, learning_rate = args.learning_rate, 
                distribution = args.init_distribution, parameter_shift_rule = args.shift_rule,
                snapshot_dir= 'run' + str(path_count))
    qgan.run(quantum_instance)

    # Visualize the results
    count = 1
    if args.backend_name == 'statevector_simulator':
        figname = 'su_yeon_v3_statevector_simulator_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
            str(args.num_epochs) + '_run' + str(count)
        while os.path.isfile('garbage/' + figname + '.pdf'):
            count += 1
            figname = 'su_yeon_v3_statevector_simulator_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_run' + str(count)
    else:
        if args.backend_name == 'qasm_simulator':
            figname_raw = 'su_yeon_v3_' + args.backend_name + '_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots) + '_run'
        elif ro_noise_only:
            figname_raw = 'su_yeon_v3_' + args.backend_name + '_readout_noise_only_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots) + '_run'
        else:
            figname_raw = 'su_yeon_v3_' + args.backend_name + '_full_noise_' + args.init_distribution + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots) + '_run'
        figname = figname_raw + str(count)
        while os.path.isfile('garbage/' + figname + '.pdf'):
            count += 1
            figname = figname_raw + str(count)

    plot_results(X_train, qgan._generator, quantum_instance, qgan.g_loss,
                 qgan.d_loss, qgan.rel_entr, bounds, args.num_epochs, figname=figname)
