from __future__ import print_function


import os
#os.environ['LD_LIBRARY_PATH'] = os.getcwd()
#print(os.environ)


import h5py 
import numpy as np
from math import floor

import matplotlib.pyplot as plt

def load_training_data(file="/data/suchang/data/Electron2D_data.h5"):
    "Load training data"
    nevt = 10000  # Number of events for training
    d = h5py.File(file, 'r')

    # the data consists of two items, the ECAL shower profile which is 200k x 1 x 25 x 25 and
    # the initial electron energy (name = 'target') that is a uniform distribution between [0,500] GeV.
    # we use only the ECAL shower profile and not the energy as input

    xd = d.get('ECAL')
    nx = xd.shape[2]
    ny = xd.shape[3]
    X = np.array(xd[:nevt, :, :])

    return X


def average_data(X, n=3, N=30000):
    x_sum = []
    for x in X:
        tmp = np.sum(x[0], axis=0)
        x_sum.append([np.sum(tmp[3*i:3*(i+1)])
                      for i in range(floor(len(tmp)/3))])
        if np.sum(x_sum[-1]) != 0:
            x_sum[-1] /= np.sum(x_sum[-1])

    real_dist = np.mean(x_sum, axis=0)
    real_pdf = np.array([np.sum(real_dist[0:i+1])
                         for i in range(len(real_dist))])
    real_pdf[-1] = 1

    # Create training set of scalars following the real energy distribution
    X_train = []
    for i in range(N):
        num = np.argwhere(real_pdf > np.random.uniform(0, 1))[0][0]
        if num == 0:
            X_train.append(np.random.uniform(0, 0.5))
        elif num == 7:
            X_train.append(np.random.uniform(6.5, 7))
        else:
            X_train.append(np.random.uniform(num-0.5, num+0.5))

    X_train = np.array(X_train)

    # Set the data resolution
    # Set upper and lower data values as list of k min/max data values [[min_0,max_0],...,[min_k-1,max_k-1]]
    # In this example, k = 1
    bounds = np.array([0., 2**n - 1])

    return X_train, real_dist, bounds


def create_folder(folder_name, print_outputs = True):        #create folders in which the trainings progress will be saved
    dirName=folder_name
    try:
        # Create target Directory
        os.mkdir(dirName)
        if print_outputs == True:
            print("Directory " , dirName ,  " Created ") 
    except FileExistsError:
        if print_outputs == True:
            print("Directory " , dirName ,  " already exists")
    return

def loss_plots(save_folder, epoch, g_loss, d_loss):  #timestep = epoch
    # Plot progress w.r.t the generator's and the discriminator's loss function
    t_steps = np.arange(epoch)
    #print(epoch, t_steps, g_loss)
    plt.figure(figsize=(6,5))
    plt.title("Loss Function", fontsize=16)
    plt.plot(t_steps, g_loss, label = "Generator Loss", color = 'mediumvioletred', linewidth = 2)
    plt.plot(t_steps, d_loss, label = "Discriminator Loss", color = 'rebeccapurple', linewidth = 2)
    plt.grid()
    plt.legend(loc = 'best', fontsize=14)
    plt.xlabel('epochs', fontsize=14)
    plt.ylabel('loss', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.savefig(os.path.join(save_folder, "loss_plot.png"))
    if epoch%50 == 0:
        plt.show()
    plt.close()
    return

def relative_entropy_plot(save_folder, rel_entr, epoch):
    # Plot progress w.r.t relative entropy
    t_steps = np.arange(epoch)
    plt.figure(figsize=(6,5))
    plt.title("Relative Entropy ", fontsize=16)
    plt.plot(t_steps, rel_entr, color ='mediumblue', lw=4, ls=':')
    plt.grid()
    plt.xlabel('epochs', fontsize=14)
    plt.ylabel('relative entropy', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.savefig(os.path.join(save_folder, "relative_entropy.png"))
    if epoch%50 == 0:
        plt.show()
    plt.close()
    return

def PDF_plot(save_folder, generator, quantum_instance, X_train, bounds, epoch):
    #Plot the PDF of the resulting distribution against the target distribution
    plt.figure(figsize=(6,5))
    plt.title("PDF", fontsize=16)
    samples_g, prob_g = generator.get_output(quantum_instance, shots=10000)


    r = np.round(X_train)
    r = r[r <= bounds[1]]
    temp = []
    for i in range(int(bounds[1]+1)):
        temp += [np.sum(r==i)]
    r = np.array(temp / sum(temp))

    samples_g = np.array(samples_g)
    samples_g = samples_g.flatten()
    num_bins = len(prob_g)
    plt.bar(samples_g,  prob_g, color='royalblue', width= 0.8, label='qGAN')
    plt.plot(r,'-o', label='Geant4', color='deepskyblue', linewidth=4, markersize=12)

    plt.xticks(np.arange(min(samples_g), max(samples_g)+1, 1.0))
    plt.grid()
    plt.xlabel('x', fontsize=14)
    plt.ylabel('p(x)', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.ylim([0,0.25])
    plt.legend(loc='best', fontsize=14)
    if epoch%100 == 0 or epoch == 1:
        plt.savefig(os.path.join(save_folder, str(epoch) + "PDF_plot.png"))
    if epoch%50 == 0 or epoch == 1:
        plt.show()
    plt.close()
    return


def plot_results(data, generator, quantum_instance, g_loss, d_loss, rel_entr, bounds, num_epochs, figname=""):
    "Visualize final results"

    t_steps = np.arange(num_epochs)

    plt.figure(figsize=(18, 5))

    plt.subplot(131)
    t_steps = np.arange(num_epochs)
    plt.title("Progress in the loss function")
    plt.plot(t_steps, g_loss, label="Generator loss function", linewidth=2)
    plt.plot(t_steps, d_loss,
             label="Discriminator loss function", linewidth=2)
    plt.grid()
    plt.legend(loc='best')
    plt.xlabel('time steps')
    plt.ylabel('loss')

    # Plot progress w.r.t relative entropy
    plt.subplot(132)
    plt.title("Relative Entropy ")
    plt.plot(t_steps, rel_entr, lw=2)
    plt.grid()
    plt.xlabel('time steps')
    plt.ylabel('relative entropy')

    # Plot the PDF of the resulting distribution against the target distribution
    plt.subplot(133)
    plt.title("CDF")
    samples_g, prob_g = generator.get_output(quantum_instance, shots=10000)
    r = np.round(data)
    r = r[r <= bounds[1]]
    temp = []
    for i in range(int(bounds[1]+1)):
        temp += [np.sum(r == i)]
    r = np.array(temp / sum(temp))

    samples_g = np.array(samples_g)
    samples_g = samples_g.flatten()
    plt.bar(samples_g,  prob_g, width=0.8, label='simulation')
    plt.plot(r, '--o', label='log-normal', linewidth=2,
             markersize=12, color='tab:orange')

    plt.xticks(np.arange(min(samples_g), max(samples_g)+1, 1.0))
    plt.grid()
    plt.xlabel('x')
    plt.ylabel('p(x)')
    plt.legend(loc='best')

    # Some beautyfication and save final results
    plt.tight_layout()
    if figname:
        plt.savefig('garbage/' + figname + '.pdf')
    # plt.show()
    plt.close("all")
