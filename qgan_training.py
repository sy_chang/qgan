from qiskit.utils import QuantumInstance
import numpy as np
import os 
import csv
import time

from scipy.stats import entropy

from qiskit.aqua.components.neural_networks.pytorch_discriminator import PyTorchDiscriminator
from qiskit.aqua import aqua_globals


from typing import Optional, List, Union, Dict, Any

import numpy as np

from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit
from qiskit.circuit.library import TwoLocal
from qiskit.aqua import aqua_globals
from qiskit.aqua.components.optimizers import ADAM
from qiskit.aqua.components.optimizers import Optimizer
from qiskit.aqua.components.uncertainty_models import UnivariateVariationalDistribution, \
    MultivariateVariationalDistribution

from math import pi
import json


from qiskit.aqua.components.optimizers import ADAM
from qiskit.circuit.library import TwoLocal, UniformDistribution, NormalDistribution


# pylint: disable=invalid-name


class QuantumGenerator():
    def __init__(self,
                 bounds: np.ndarray,
                 num_qubits: List[int],
                 generator_circuit: Optional[Union[UnivariateVariationalDistribution,
                                                   MultivariateVariationalDistribution,
                                                   QuantumCircuit]] = None,
                 init_params: Optional[Union[List[float], np.ndarray]] = None,
                 optimizer: Optional[Optimizer] = None,
                 snapshot_dir: Optional[str] = None) -> None:


        super().__init__()

        self._bounds = bounds
        self._num_qubits = num_qubits
        self.generator_circuit = generator_circuit
        if generator_circuit is None:
            circuit = QuantumCircuit(sum(num_qubits))
            circuit.h(circuit.qubits)
            var_form = TwoLocal(sum(num_qubits), 'ry', 'cz', reps=1, entanglement='circular')
            circuit.compose(var_form, inplace=True)

            # Set generator circuit
            self.generator_circuit = circuit

        if init_params is None:
            init_params = aqua_globals.random.random(self.generator_circuit.num_parameters) * 2e-2

        self._num_parameters = len(init_params)
        self._bound_parameters = init_params

        # Set optimizer for updating the generator network

        if optimizer:
            self._optimizer = optimizer
        else:
            self._optimizer = ADAM(maxiter=1, tol=1e-6, lr=1e-3, beta_1=0.7,
                                   beta_2=0.99, noise_factor=1e-6,
                                   eps=1e-6, amsgrad=True, snapshot_dir=snapshot_dir)


        self._data_grid = np.linspace(bounds[0], bounds[1], (2 ** self._num_qubits[0]))
        self._shots = None
        self._discriminator = None
        self._ret = {}  # type: Dict[str, Any]
    
    def construct_circuit(self, params=None):
        """
        Construct generator circuit.

        Args:
            params (list | dict): parameters which should be used to run the generator.

        Returns:
            Instruction: construct the quantum circuit and return as gate
        """
        if params is None:
            return self.generator_circuit

        if isinstance(params, (list, np.ndarray)):
            params = dict(zip(self._free_parameters, params))

        return self.generator_circuit.assign_parameters(params)

    def get_grad_fn(self, quantum_instance, discriminator, shots = None ):
        def grad_fn(x_center):
            generated_data, generated_prob = self.get_output(quantum_instance, x_center, shots)
            prediction_generated = discriminator.get_label(generated_data, detach=True)

            params_plus = np.repeat(np.array([x_center]), len(x_center), axis = 0)
            params_minus = np.repeat(np.array([x_center]), len(x_center), axis = 0)


            for i in range(len(x_center)):
                params_plus[i,i] += pi/2
                params_minus[i,i] -= pi/2


            p_plus = []
            p_minus = []

            for x in params_plus :
                _, prob= self.get_output(quantum_instance, x, shots)
                p_plus.append(prob)

            for x in params_minus:
                _, prob= self.get_output(quantum_instance, x, shots)
                p_minus.append(prob)

            gradients = []
            for i in range(len(x_center)):
                gradients.append(np.sum(np.array([(p_minus[i][j]-p_plus[i][j])
                                                  *np.log(prediction_generated[j])/2
                                                  for j in range(len(prediction_generated))])))

            return np.array(gradients)
        return grad_fn

    def get_output(self, quantum_instance, params=None, shots=None):
        """
        Get classical data samples from the generator.
        Running the quantum generator circuit results in a quantum state.
        To train this generator with a classical discriminator, we need to sample classical outputs
        by measuring the quantum state and mapping them to feature space defined by the training
        data.

        Args:
            quantum_instance (QuantumInstance): Quantum Instance, used to run the generator
                circuit.
            params (numpy.ndarray): array or None, parameters which should
                be used to run the generator, if None use self._params
            shots (int): if not None use a number of shots that is different from the
                number set in quantum_instance

        Returns:
            list: generated samples, array: sample occurrence in percentage
        """
#         print("params = ", params)
        instance_shots = quantum_instance.run_config.shots
        q = QuantumRegister(sum(self._num_qubits), name='q')
        qc = QuantumCircuit(q)

        if params is None:
            params = self._bound_parameters
        qc.append(self.construct_circuit(params), q)
        if quantum_instance.is_statevector:
            pass
        else:
            c = ClassicalRegister(sum(self._num_qubits), name='c')
            qc.add_register(c)
            qc.measure(q, c)

        if shots is not None:
            quantum_instance.set_config(shots=shots)

        result = quantum_instance.execute(qc)

        generated_samples = []
        if quantum_instance.is_statevector:
            result = result.get_statevector(qc)
            values = np.multiply(result, np.conj(result))
            values = list(values.real)

        else:
            result = result.get_counts(qc)
            keys = list(result)
            temp = list(result.values())
            temp = [float(v) / np.sum(temp) for v in temp]
            values = [0.0 for _ in range(2**self._num_qubits[0])]

            for j, b in enumerate(keys) :
                values[int(b, 2)] = temp[j]

        generated_samples_weights = values

        generated_samples = [[x] for x in self._data_grid]

        # self.generator_circuit._probabilities = generated_samples_weights
        if shots is not None:
            # Restore the initial quantum_instance configuration
            quantum_instance.set_config(shots=instance_shots)
        return generated_samples, generated_samples_weights

    def loss(self, x, weights):  # pylint: disable=arguments-differ
        """
        Loss function for training the generator's parameters.

        Args:
            x (numpy.ndarray): sample label (equivalent to discriminator output)
            weights (numpy.ndarray): probability for measuring the sample

        Returns:
            float: loss function
        """
        try:
            # pylint: disable=no-member
            loss = (-1) * np.dot(np.log(x).transpose(), weights)
        except Exception:  # pylint: disable=broad-except
            loss = (-1) * np.dot(np.log(x), weights)
        return loss.flatten()

    def _get_objective_function(self, quantum_instance, discriminator):
        def objective_function(params):
            generated_data, generated_prob = self.get_output(quantum_instance, params=params,
                                                             shots= self._shots)
            prediction_generated = discriminator.get_label(generated_data, detach=True)
            return self.loss(prediction_generated, generated_prob)

        return objective_function

    def train(self, discriminator, quantum_instance=None, shots=None, PS = False):
        self._shots = shots
        objective = self._get_objective_function(quantum_instance, discriminator)

        if PS :
            grad_fn = self.get_grad_fn(quantum_instance, discriminator, shots)

            self._bound_parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._bound_parameters),
                objective_function=objective,
                gradient_function = grad_fn,
                initial_point=self._bound_parameters
                )
        else :
            self._bound_parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._bound_parameters),
                objective_function=objective,
                initial_point=self._bound_parameters
                )

        self._ret['loss'] = loss
        self._ret['params'] = self._bound_parameters

        return self._ret
        

def main(backend, user_messenger, X_train, num_epochs, num_shots, lrate_g, lrate_d, decay_rate, snapshot_dir):
    def l_dec(initial_lrate, epoch, start_decay=500, decay_rate=0.001):
        epoch = epoch - 1 #because training starts at epoch 1
        if epoch < start_decay:
            k = 0.0
        else:
            k = decay_rate #0.07
            epoch = epoch - start_decay
        lrate = initial_lrate * np.exp(-k*epoch)
        return lrate
    bounds = [0, 8]
    depth = 2
    num_qubits = [3]

    "Set up the quantum generator and the classical discriminator"

    # Set entangler map
    # In this example, we choose linear entangler map
    entangler_map = 'linear'
    init_dist = UniformDistribution(np.sum(num_qubits))

    # Set RY variational form with chosen initial state
    var_form = TwoLocal(int(np.sum(num_qubits)), rotation_blocks='ry',
                        entanglement_blocks='cz', entanglement=entangler_map, reps=depth)

    # Set generator's initial parameters
    init_params = np.random.rand(var_form.num_parameters_settable) * 0.2 - 0.1

    # Set generator circuit by adding the initial distribution in front of the ansatz
    g_circuit = var_form.compose(init_dist, front=True)

    # The optimizer we are going to use
    optim_g = ADAM(maxiter=1, tol=1e-6, lr=1e-4, beta_1=0.7, beta_2=0.99,
                   noise_factor=1e-6, eps=1e-6, amsgrad=True, snapshot_dir=snapshot_dir)

    # Create a quantum generator
    generator = QuantumGenerator(
        bounds, num_qubits, g_circuit, init_params, optim_g, snapshot_dir)
    generator._free_parameters = sorted(
        g_circuit.parameters, key=lambda p: p.name)




    X_train = np.array(X_train)
    quantum_instance = QuantumInstance(backend)
    discriminator = PyTorchDiscriminator(len(num_qubits))

    
    batch_size = 3000
    parameter_shift_rule = True
    real_dist = np.mean(X_train, axis =0)
    bounds = [0,2**np.sum(num_qubits)]
    # Lists for monitoring the loss functions and the relative entropy
    d_loss = []
    g_loss = []
    rel_entr = []

    # Take a snapshot
    if snapshot_dir is not None:
        with open(os.path.join(snapshot_dir, 'output.csv'), mode='w') as csv_file:
            fieldnames = ['epoch', 'loss_discriminator', 'loss_generator', 'params_generator', 'generated_prob',
                          'rel_entropy']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

    # Run qGAN
    start = time.time()
    start_decay = 100

#    for e in tqdm(range(num_epochs)):
    
    for e in range(num_epochs):
        aqua_globals.random.shuffle(X_train)
        index = 0

        generator._optimizer._lr = l_dec(lrate_g, e, start_decay=start_decay, decay_rate=decay_rate)
        discriminator._optimizer._lr = l_dec(lrate_d, e, start_decay=start_decay, decay_rate=decay_rate)


        generated_batch, generated_prob = generator.get_output(
            quantum_instance, shots=num_shots)
        
        while (index + batch_size) <= len(X_train):
            real_batch = X_train[index: index + batch_size]
            index += batch_size

            # 1. Train Discriminator
            ret_d = discriminator.train([real_batch, generated_batch], [
                                        np.ones(len(real_batch)) / len(real_batch), generated_prob])
            d_loss_min = ret_d['loss']

            # 2. Train Generator
            ret_g = generator.train(
                discriminator, quantum_instance, shots=num_shots, PS=parameter_shift_rule)
            g_loss_min = ret_g['loss']

            generated_batch, generated_prob = generator.get_output(
                quantum_instance, shots=num_shots)

        d_loss.append(float(d_loss_min))
        g_loss.append(g_loss_min)
        rel = entropy(
            np.array([1e-8 if x == 0 else x for x in generated_prob]), real_dist)
        rel_entr.append(rel)

        if snapshot_dir is not None:
            with open(os.path.join(snapshot_dir, 'output.csv'), mode='a') as csv_file:
                fieldnames = ['epoch', 'loss_discriminator',
                              'loss_generator', 'params_generator', 'generated_prob', 'rel_entropy']
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                writer.writerow({'epoch': e, 'loss_discriminator': np.average(np.around(d_loss_min, 4)),
                                 'loss_generator': np.average(np.around(g_loss_min, 4)),
                                 'params_generator': generator._bound_parameters,
                                 'generated_prob': generated_prob,
                                 'rel_entropy': np.around(rel, 4)})
            discriminator.save_model(snapshot_dir)  # Store discriminator model

        #plots
        # if e%1 == 0:
        #     loss_plots(snapshot_dir, e+1, g_loss, d_loss)
        #     relative_entropy_plot(snapshot_dir, rel_entr, e+1)
        #     PDF_plot(snapshot_dir, generator, quantum_instance, X_train, bounds, e+1)

        print('Epoch %s/%s, D_loss: %.4f, G_loss: %.4f, Rel_entr: %.4f' % (e + 1, num_epochs,
                                                                       np.around(float(d_loss_min), 4), np.around(g_loss_min, 4), np.around(rel, 4)))

    end = time.time()
    # Runtime
    print('qGAN training runtime: ', (end - start)/60., ' min')

    
    user_messenger.publish({"g_loss" : g_loss, "d_loss" : d_loss, "rel_entr": rel_entr, 'time_taken': (end - start)/60.})

    return g_loss, d_loss, rel_entr